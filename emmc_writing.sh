#!/bin/sh
#
# Copyright (C) 2023 Inbic Co,. Ltd
# Jongju Park <jongju0920@gmail.com>
#
# SPDX-License-Identifier:      GPL-2.0+
#

IMG="etri_n2.img"
OUTPUT="/dev/mmcblk0"

# SD CARD 
# sudo dd if="$IMG" of="OUTPUT" conv=fsync,notrunc bs=512 seek=1

# EMMC
dd if=/odroid.img of=/dev/mmcblk0 bs=2M status=progress conv=sync
sync

# LED ON
echo default-on > /sys/class/leds/blue\:heartbeat/trigger
echo "EMMC Wwriting Finished."